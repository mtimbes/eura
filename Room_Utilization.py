import numpy as np
import pandas as pd
from io import StringIO

E = .95 #Efficiency goal


class room_assign:
    def __init__(self,room_name,class_name,need,rcap,eff):
        self.room   = str(room_name)
        self.course = str(class_name)
        self.cneed  = str(need)
        self.rocap  = str(rcap)
        self.score = str(eff)
    def disp(self):
        output = " "
        output = output + str("COURSE: " + self.course + " ")
        output = output + str("ROOM: " + self.room + " ")
        output = output + str("CAP_NEED: " + self.cneed + " ")
        output = output + str("ROOM CAP: " + self.rocap + " ")
        output = output + str("Efficiency: " + self.score + " ")
        return output
def poss_sol(room_cap,cap_need,class_info,room_info,E):
    '''
    Input: room_cap(1-D arr),cap_need(1-D arr), class_info(1-D arr), room_info(1-D arr),E(float >1.0)
    Return: Array of type room_assign. Suggestions on what room to assign.
    '''
    solutions = [] #Empty List
    print("Finding Solutions...")
    for i in range(0,len(cap_need)):
        for j in range(0,len(room_cap)):
            if(room_cap[j] >= cap_need[i] and (cap_need[i]/room_cap[j]) >= E):
                #print(" Match Found: ",room_info[j],class_info[i])
                solutions.append(room_assign(room_info[j],class_info[i],cap_need[i],room_cap[j],cap_need[i]/room_cap[j])) 
                #Mark Used Room
                #room_cap[i] = np.inf
                room_cap[i] = 1000000000000
                break
            else:
                continue 
    return solutions

def find_cap(num_students, ideal_E):
    '''
    Input: num_students(1-D arr),ideal_E(float < 1.0)
    Return: Ideal room capacity.
    '''
    return (num_students*ideal_E)

#   Read Classroom Data
data_frame =pd.read_csv('roomdata.csv', sep=',',header=None)
#   Convert to NP Array --> ['ROOM NAME' 'CAPACITY']
capactiy_data = np.asarray(data_frame.values[:,1],float)
classroom_data = np.asarray(data_frame.values[:,0],str)
num_rooms = len(classroom_data)
#
#   Read Course and Student Data
data_frame =pd.read_csv('studentdata.csv', sep=',',header=None)
#   Convert to NP Array --> ['CLASS NAME' 'ENROLLMENT']
numstud_data = np.asarray(data_frame.values[:,1],float)
course_data = np.asarray(data_frame.values[:,0],str)
num_classes = len(numstud_data)
print('Number of courses to be paired:',num_classes)
print('Number of classrooms available:',num_rooms)
#   Find Ideal Capacity
cap_need = find_cap(numstud_data,E) #Find ideal capacity
cap_need = np.ceil(cap_need) # Round up
#print(cap_need)
#Create copy of capacity_data for later update
capacity = np.copy(capactiy_data)
# Possible Solutions cast from list to nparray
out_list = []
result = np.asarray(poss_sol(capacity,cap_need,course_data,classroom_data,E))
for i in range(0,len(result)):
    out_list.append(result[i].disp())
out_frame= pd.DataFrame(out_list)
out_frame.to_csv("output.csv", sep = ',')